﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Allied.Users.Controllers
{
    public class ResponseWriter
    {
        public static void WriteJson(HttpResponseBase response, ApiResponse resp) {
            response.Clear();
            response.ContentType = ContentType.Json;
            response.Write(resp.ToJson());
            response.End();
        }
    }
}