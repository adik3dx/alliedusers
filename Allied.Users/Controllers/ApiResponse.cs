﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using System.Net;

namespace Allied.Users.Controllers
{
    public class ApiResponse
    {
        public Int32 code;
        public String type;
        public Object data;
        public ApiResponse(Object data) {
            this.data = data;
        }

        public ApiResponse Type(String type) {
            this.type = type;
            return this;
        }

        public ApiResponse Code(HttpStatusCode code)
        {
            this.code = (int)code;
            return this;
        }

        public String ToJson() {
            return JsonConvert.SerializeObject(this);
        }
    }
}