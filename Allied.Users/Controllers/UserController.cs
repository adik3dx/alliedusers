﻿using Allied.Users.Models.Users;
using System;
using System.Web.Mvc;
using System.Net;
using System.Collections.Generic;

namespace Allied.Users.Controllers
{
    [RoutePrefix("user")]
    public class UserController : Controller
    {
        [HttpPost]
        [Route("")]
        public void Register()
        {
            User user = Models.Users.User.parseRequest(Request);
            if (user == null)
            {
                ResponseWriter.WriteJson(Response, new ApiResponse(Messages.InvalidInput)
                    .Code(HttpStatusCode.BadRequest).Type(Types.Failure));
                return;
            }

            user.Id = 0;
            if (String.IsNullOrEmpty(user.Email) || UserEntityController.UserExists(user.Email))
            {
                ResponseWriter.WriteJson(Response, new ApiResponse(Messages.UserExists)
                    .Code(HttpStatusCode.Conflict).Type(Types.Failure));
                return;
            }

            User createdUser = UserEntityController.CreateUser(user);

            if (createdUser.Id > 0)
            {
                ResponseWriter.WriteJson(Response, new ApiResponse(Messages.RegistrationSuccess)
                    .Code(HttpStatusCode.OK).Type(Types.Message));
                return;
            }

            ResponseWriter.WriteJson(Response, new ApiResponse(Messages.SomethingWentWrong)
                .Code(HttpStatusCode.InternalServerError).Type(Types.Failure));
        }

        
        [HttpPut]
        [Route("")]
        public void Edit() {
            User user = Models.Users.User.parseRequest(Request);
            if (user == null)
            {
                ResponseWriter.WriteJson(Response, new ApiResponse(Messages.InvalidInput)
                    .Code(HttpStatusCode.BadRequest).Type(Types.Failure));
                return;
            }

            if (UserEntityController.UserExists(user.Email))
            {
                ResponseWriter.WriteJson(Response, new ApiResponse(Messages.UserExists)
                    .Code(HttpStatusCode.OK).Type(Types.Failure));
                return;
            }

            if (user.Id == 0) {
                ResponseWriter.WriteJson(Response, new ApiResponse(Messages.IdError)
                    .Code(HttpStatusCode.BadRequest).Type(Types.Failure));
                return;
            }

            if (UserEntityController.UserExists(user.Id) == false) {
                ResponseWriter.WriteJson(Response, new ApiResponse(Messages.UserDoesNotExists)
                    .Code(HttpStatusCode.NotFound).Type(Types.Failure));
                return;
            }

            UserEntityController.Update(user);
            ResponseWriter.WriteJson(Response, new ApiResponse(Messages.UserUpdateSuccess)
                .Code(HttpStatusCode.OK).Type(Types.Message));
        }

        [HttpGet]
        [Route("{userId}")]
        public void Get(String userId)
        {
            Int64 id = 0;
            Int64.TryParse(userId, out id);
            if (id <= 0)
            {
                ResponseWriter.WriteJson(Response, new ApiResponse(Messages.IdError)
                    .Code(HttpStatusCode.BadRequest).Type(Types.Failure));
                return;
            }

            User user = UserEntityController.Get(id);
            if (user == null) {
                ResponseWriter.WriteJson(Response, new ApiResponse(Messages.UserDoesNotExists)
                    .Code(HttpStatusCode.NotFound).Type(Types.Failure));
                return;
            }

            ResponseWriter.WriteJson(Response, new ApiResponse(user)
                    .Code(HttpStatusCode.OK).Type(Types.Data));
        }

        [HttpGet]
        [Route("")]
        public void Select()
        {
            string tag = Request["tag"];
            if (String.IsNullOrEmpty(tag))
            {
                ResponseWriter.WriteJson(Response, new ApiResponse(Messages.InvalidInput)
                    .Code(HttpStatusCode.BadRequest).Type(Types.Failure));
                return;
            }

            List<User> userList = UserEntityController.SelectByTag(tag);
            ResponseWriter.WriteJson(Response, new ApiResponse(userList)
                .Code(HttpStatusCode.OK).Type(Types.Data));
        }

        [HttpDelete]
        [Route("{userId}")]
        public void Delete(string userId)
        {
            Int64 id = 0;
            Int64.TryParse(userId, out id);
            if (id <= 0)
            {
                ResponseWriter.WriteJson(Response, new ApiResponse(Messages.IdError)
                    .Code(HttpStatusCode.BadRequest).Type(Types.Failure));
                return;
            }

            if (UserEntityController.Delete(id))
            {
                ResponseWriter.WriteJson(Response, new ApiResponse(Messages.UserWasDeleted)
                    .Code(HttpStatusCode.OK).Type(Types.Message));
                return;
            }

            ResponseWriter.WriteJson(Response, new ApiResponse(Messages.UserDoesNotExists)
                    .Code(HttpStatusCode.NotFound).Type(Types.Failure));
        }
    }
}