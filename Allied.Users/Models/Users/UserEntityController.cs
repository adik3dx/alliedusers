﻿using System;
using System.Linq;
using System.Web.Helpers;
using System.Data;
using System.Collections.Generic;
using System.Text;
using System.Globalization;
using System.Data.SqlClient;
using System.Data.Common;

namespace Allied.Users.Models.Users
{
    public class UserEntityController
    {
        private static AppDbContext db = new AppDbContext();

        public static bool UserExists(string email)
        {
            return db.Users.Where(user => user.Email.Equals(email)).Count() > 0;
        }

        public static bool UserExists(Int64 id)
        {
            return db.Users.Where(user => user.Id == id).Count() > 0;
        }

        private static string HashPassword(string password)
        {
            if (String.IsNullOrEmpty(password)) {
                return String.Empty;
            }

            return Crypto.HashPassword(password);
        }
        public static bool VerifyPassword(string hashedPassword, string password)
        {
            return Crypto.VerifyHashedPassword(hashedPassword, password);
        }
        public static User CreateUser(User user)
        {
            user.Password = HashPassword(user.Password);

            db.Users.Add(user);

            db.SaveChanges();

            return user;
        }
        public static User SelectUserByEmail(string email)
        {
            return db.Users.Where(user => user.Email.Equals(email)).FirstOrDefault();
        }

        public static void Update(User user)
        {
            user.Password = HashPassword(user.Password);
            User userFromDb = db.Users.Find(user.Id);
            userFromDb.merge(user);
            db.Users.Attach(userFromDb);
            db.Entry(userFromDb).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
        }

        public static User Get(Int64 id) {
            return db.Users.Find(id);
        }

        public static bool Delete(Int64 id)
        {
            User user = db.Users.Find(id);
            if (user != null) {
                db.Users.Remove(user);
                db.SaveChanges();
                return true;
            }

            return false;
        }

        public static List<User> SelectByTag(string tag)
        {
            string query = @"select users.* from dbo.users as users
                             inner join dbo.tags as tags on tags.id = users.tag
                             and tags.name LIKE @tag";
            return db.Database.SqlQuery<User>(query, new SqlParameter("@tag", tag)).ToList();
        }
    }
}