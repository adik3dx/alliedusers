﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using Newtonsoft.Json;

namespace Allied.Users.Models.Users
{
    public class User
    {
        public Int64 Id { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string Password { get; set; }
        public Int32 UserStatus { get; set; }

        public Int64 tag { get; set; }

        public void merge(User user) {
            this.Email = String.IsNullOrEmpty(user.Email) ? this.Email : user.Email;
            this.FirstName = String.IsNullOrEmpty(user.FirstName) ? this.FirstName : user.FirstName;
            this.LastName = String.IsNullOrEmpty(user.LastName) ? this.LastName : user.LastName;
            this.Password = String.IsNullOrEmpty(user.Password) ? this.Password : user.Password;
            this.Phone = String.IsNullOrEmpty(user.Phone) ? this.Phone : user.Phone;
            this.Username = String.IsNullOrEmpty(user.Username) ? this.Username : user.Username;
            this.UserStatus = user.UserStatus < 0 ? this.UserStatus : user.UserStatus;
            this.tag = user.tag < 1 ? this.tag : user.tag;
        }

        public static User parseRequest(HttpRequestBase request)
        {
            //Int32 userStatus = 0;
            //Int32.TryParse(request["username"], out userStatus);
            //Int64 id = 0;
            //Int64.TryParse(request["id"], out id);

            using (StreamReader reqStream = new StreamReader(request.InputStream))
            {
                string body = reqStream.ReadToEnd();
                try
                {
                    return JsonConvert.DeserializeObject<User>(body);
                }
                catch (Exception ex)
                {
                    return null;
                }
            }

            //return new User()
            //{
            //    Email = request["email"],
            //    Username = request["username"],
            //    Password = request["password"],
            //    FirstName = request["firstName"],
            //    LastName = request["lastName"],
            //    Phone = request["phone"],
            //    UserStatus = userStatus,
            //    Id = id,
            //};
        }
    }
}