﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using Allied.Users.Models.Users;
using Allied.Users.Models.Tags;

namespace Allied.Users.Models
{
    public class AppDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<Tag> Tags { get; set; }

        public AppDbContext() : base(nameOrConnectionString: @"md089\sqlexpress.test.dbo") {


        }
    }
}