﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Allied.Users.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Allied.UsersTests;
using System.Threading;
using System.Net.Http;
using System.Net;
using Allied.UsersTests.Controllers.Test;
using Allied.Users.Models.Users;
using Newtonsoft.Json;
using Allied.UsersTests.Controllers;
using System.Data.Entity;
using Allied.Users.Models;
using System.Data.SqlClient;
using Newtonsoft.Json.Linq;

namespace Allied.Users.Controllers.Tests
{
    [TestClass()]
    public class UserControllerTests
    {
        private AppDbContext db = new AppDbContext();
        private String baseUrl = "http://localhost:50607/";
        private TestMain testMain;
        private String initialEmail = "adik3dx@mail.prz";
        private String changedEmail = "ado@mail.prz";

        [TestMethod]
        public void Test()
        {
            this.db.Database.Initialize(false);
            this.db.Database.Connection.Open();
            this.testMain = new TestMain();
            this.testMain.StartServer();
            Thread.Sleep(1000);
            try {
                this.RegisterTest();
                this.EditTest();
                this.GetTest();
                this.SelectTest();
                this.Delete();
            }
            finally
            {
                try {
                    this.testMain.StopServer();
                }
                catch (Exception e)
                {

                }
            }
        }

        public void RegisterTest()
        {

            User user = new User()
            {
                Username = "adik3dx",
                FirstName = "Eduard",
                LastName = "Baranetsky",
                Password = "123",
                Phone = "000235",
                Email = this.initialEmail,
                tag = 1,
            };

            String response = HttpWorker.MakePost(user, baseUrl + "user");
            
            ApiResponse apiResp = JsonConvert.DeserializeObject<ApiResponse>(response);
            Assert.AreEqual((int)HttpStatusCode.OK, apiResp.code);
        }

        public void EditTest()
        {
            User userFromDb = this.db.Users.Where(u => u.Email.Equals(this.initialEmail)).First();
            
            User user = new User()
            {
                Email = changedEmail,
                Id = userFromDb.Id,
            };

            string response = HttpWorker.MakePut(user, baseUrl + "user");
            ApiResponse apiResp = JsonConvert.DeserializeObject<ApiResponse>(response);
            Assert.AreEqual((int)HttpStatusCode.OK, apiResp.code);
            
            Assert.AreNotEqual(this.db.Users.Where(u=>u.Email.Equals(changedEmail)).Count(), 0);

        }

        public void GetTest()
        {
            User userFromDb = this.db.Users.Where(u => u.Email.Equals(this.changedEmail)).First();
            string response = HttpWorker.MakeGet(baseUrl + "user/"+userFromDb.Id);
            ApiResponse apiResp = JsonConvert.DeserializeObject<ApiResponse>(response);
            Assert.AreEqual((int)HttpStatusCode.OK, apiResp.code);
            User userFromResponse = ((JObject)apiResp.data).ToObject<User>();
            Assert.AreEqual(userFromResponse.Id, userFromDb.Id);
        }

        public void SelectTest()
        {
            HttpWorker.MakePost(new User() {
                Email = "test",
                tag = 2
            }, baseUrl + "user");

            string response = HttpWorker.MakeGet(baseUrl + "user?tag=admin");
            ApiResponse apiResp = JsonConvert.DeserializeObject<ApiResponse>(response);
            Assert.AreEqual((int)HttpStatusCode.OK, apiResp.code);
            List<User> users = ((JArray)apiResp.data).ToObject<List<User>>();
            Assert.AreEqual(users.Count, 1);
            Assert.AreEqual(users[0].Email, changedEmail);

            response = HttpWorker.MakeGet(baseUrl + "user?tag=client");
            apiResp = JsonConvert.DeserializeObject<ApiResponse>(response);
            Assert.AreEqual((int)HttpStatusCode.OK, apiResp.code);
            users = ((JArray)apiResp.data).ToObject<List<User>>();
            Assert.AreEqual(users.Count, 1);
            Assert.AreEqual(users[0].Email, "test");

            User user = db.Users.Find(users[0].Id);
            if (user != null)
            {
                db.Users.Remove(user);
                db.SaveChanges();
            }
        }

        public void Delete()
        {
            User user = this.db.Users.Where(u => u.Email.Equals(changedEmail)).First();

            string response = HttpWorker.MakeDelete(baseUrl + "user/"+user.Id);
            ApiResponse apiResp = JsonConvert.DeserializeObject<ApiResponse>(response);
            Assert.AreEqual((int)HttpStatusCode.OK, apiResp.code);

            int count = this.db.Users.Where(u => u.Email.Equals(changedEmail)).Count();
            Assert.AreEqual(count, 0);
        }
    }
}