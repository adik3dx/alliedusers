﻿using Allied.Users.Controllers;
using Allied.Users.Models.Users;
using Allied.UsersTests.Controllers.Test;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Allied.UsersTests.Controllers
{
    class HttpWorker
    {
        private class WorkerData
        {
            public String method;
            public String url;
            public Object obj;

        }

        public static string MakePost(Object obj, String url)
        {
            return Send(new WorkerData()
            {
                method = Method.post,
                url = url,
                obj = obj
            });
        }

        public static string MakePut(Object obj, String url)
        {
            return Send(new WorkerData() {
                method = Method.put,
                url = url,
                obj = obj
            });
        }

        private static string Send(WorkerData data)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(data.url);
            request.Method = data.method;
            request.ContentType = ContentType.Json;

            if (data.obj != null)
            {
                string json = JsonConvert.SerializeObject(data.obj);
                byte[] jsonBytes = Encoding.ASCII.GetBytes(json);
                request.ContentLength = jsonBytes.Length;
                using (Stream requestStream = request.GetRequestStream())
                {
                    requestStream.Write(jsonBytes, 0, jsonBytes.Length);
                }
            }

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader sr = new StreamReader(response.GetResponseStream());
            string responseString = sr.ReadToEnd();
            sr.Close();

            return responseString;
        }

        public static string MakeGet(String url)
        {
            return Send(new WorkerData() {
                url = url,
                method = Method.get
            });
        }

        public static string MakeDelete(String url)
        {
            return Send(new WorkerData()
            {
                url = url,
                method = Method.delete
            });
        }
    }
}
