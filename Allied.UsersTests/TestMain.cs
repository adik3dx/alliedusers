﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Diagnostics;
using System.Windows;

namespace Allied.UsersTests
{
    class TestMain
    {
        private System.Diagnostics.Process process = null;
        private Object locker = new object();

        public void StartServer()
        {
            lock(locker)
            {
                if (process == null)
                {
                    //c:\"Program Files (x86)"\"IIS Express"\iisexpress /path:d:\projects\Allied.Users\Allied.Users\ /port:50607
                    this.process = new Process();
                    ProcessStartInfo startInfo = new System.Diagnostics.ProcessStartInfo();
                    //startInfo.WindowStyle = ProcessWindowStyle.Hidden;
                    startInfo.FileName = "cmd.exe";

                    startInfo.Arguments = "/K c:\\\"Program Files (x86)\"\\\"IIS Express\"\\iisexpress /path:d:\\projects\\Allied.Users\\Allied.Users\\ /port:50607";
                    this.process.StartInfo = startInfo;
                    this.process.Start();

                    //process = Process.Start(new ProcessStartInfo()
                    //{
                    //    FileName = "c:\\\"Program Files (x86)\"\\\"IIS Express\"\\iisexpress.exe",
                    //    Arguments = "/path:d:\\projects\\Allied.Users\\Allied.Users\\ /port:50607",
                    //    RedirectStandardOutput = true,
                    //    UseShellExecute = false,
                    //    Verb = "runas"
                    //});

                    //process.Start();
                }
            }
        }

        public void StopServer()
        {
            if (this.process != null)
            {
                this.process.EnableRaisingEvents = true;
                
                this.process.StandardInput.Write("Q");
                this.process.StandardInput.Flush();
                Console.WriteLine("Q");
                this.process.Kill();
            }
        }
    }
}
